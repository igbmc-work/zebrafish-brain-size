# Adult zebrafish brain size analysis

## Context

Users interested in studying the size of zebrafish brains have been manually measuring the width and length of the fish head from bright-field images. An automatic pipeline would provide a much more reproducible, less biased and much fast solution.

## Analysis

### Forebrain-hindbrain distance

script: `forebrain_hindbrain_helper.ijm`

Because of the lack of useful features, traditional segmentation methods are not easily applicable to find the length of the forebrain-hindbrain axis. So, a semi-automatic script has been developed which prompts the user to define the forebrain limit as a _point_ and the back of the hindbrain (where find attach) as a _line_. The script then automatically finds the minimum distance between the point and the line, exports the defined ROIs and axis length measurements.

To use this script, follow the prompts to define input and output folders. Then select the forebrain edge by placing a _Point_, &rarr; press **SPACE** &rarr; define the back of the hindbrain by placing a _Line_ &rarr; press **SPACE** &rarr; the calculated axis will be exported automatically, and then next image will be opened.

### Distance between eyes

script: `find_distance_between_eyes.ijm`

Brain width is defined as distance between the two eye. Here a completely automatic pipeline has been developed where the user simply selects Input and Output folders and the Fiji macro segmented the two eyes and find the shortest distance between them. Then exports both the ROIs and measurements.

## Installation

In order to use the `find_distance_between_eyes.ijm` script, Biovoxxel3Dbox plugin needs to be installed. Please follow their own instructions [here](https://github.com/biovoxxel/bv3dbox#installation). It's important to notice that in order to have CLIJ and Biovoxxel installed correctly, a GPU-compatible computer needs to be used.

## How to run the Fiji macros

Simply drag-and-drop the scripts in Fiji to open them after having installed the requirements described [above](#installation). Then press **Run** and follow the prompts to define Input and Output folders.

## Utilities

- The script `shortest_distance_bewteen_2D_objects.ijm` has been adapted from [ved-sharma's collections of distance calculating macros](https://github.com/ved-sharma/Shortest_distance_between_objects). It can be used on individual images when the automatic pipeline fails and the user needs to resort to manual correction.

- The script `merge_all_csv.r` is a small utility script to merge all saved measurement files on disk by the macro to a single large dataframe. Simply define the path to the csvs and run the R script.

## Author

Marco Dalla Vecchia dallavem@igbmc.fr